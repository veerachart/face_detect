#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/objdetect/objdetect.hpp"

#include <cctype>
#include <iostream>
#include <iterator>
#include <stdio.h>
#include "geometry_msgs/PointStamped.h"

using namespace std;
using namespace cv;

static const std::string OPENCV_WINDOW = "Image window";
string cascadeName = "/home/kariya/catkin_ws/src/face_detect/haarcascade_frontalface_alt.xml";
string nestedCascadeName = "/home/kariya/catkin_ws/src/face_detect/haarcascade_eye_tree_eyeglasses.xml";

#define MAX(a,b) ((a)<(b) ? (b) : (a))
#define MIN(a,b) ((a)<(b) ? (a) : (b))

class ImageConverter
{
  ros::NodeHandle nh_;
  image_transport::ImageTransport it_;
  image_transport::Subscriber image_sub_;
  image_transport::Publisher image_pub_;
  ros::Publisher center_pub_;
  bool tryflip;
  CascadeClassifier cascade, nestedCascade;
  double scale;
  int min_face_size;
  int max_face_size;
  
public:

  static void help()
  {
    cout << "\nThis program demonstrates the cascade recognizer. Now you can use Haar or LBP features.\n"
            "This classifier can recognize many kinds of rigid objects, once the appropriate classifier is trained.\n"
            "It's most known use is for faces.\n"
            "Usage:\n"
            "./facedetect [--cascade=<cascade_path> this is the primary trained classifier such as frontal face]\n"
               "   [--nested-cascade[=nested_cascade_path this an optional secondary classifier such as eyes]]\n"
               "   [--scale=<image scale greater or equal to 1, try 1.3 for example>]\n"
               "   [--try-flip]\n"
               "   [filename|camera_index]\n\n"
            "see facedetect.cmd for one call:\n"
            "./facedetect --cascade=\"../../data/haarcascades/haarcascade_frontalface_alt.xml\" --nested-cascade=\"../../data/haarcascades/haarcascade_eye.xml\" --scale=1.3\n\n"
            "During execution:\n\tHit any key to quit.\n"
            "\tUsing OpenCV version " << CV_VERSION << "\n" << endl;
  }

  ImageConverter(int argc, char** argv)
    : it_(nh_)
  {
    // Subscrive to input video feed and publish output video feed
    image_sub_ = it_.subscribe("/camera/image_raw", 1, 
      &ImageConverter::imageCb, this);
    image_pub_ = it_.advertise("/image_converter/output_video", 1);
    center_pub_ = nh_.advertise<geometry_msgs::PointStamped>("/center",30);
    CvCapture* capture = 0;
    min_face_size = 40;
    max_face_size = 100;
    
    /*const string scaleOpt = "--scale=";
    const string cascadeOpt = "--cascade=";
    const string nestedCascadeOpt = "--nested-cascade";
    const string tryFlipOpt = "--try-flip";
    size_t scaleOptLen = scaleOpt.length();
    size_t cascadeOptLen = cascadeOpt.length();
    size_t nestedCascadeOptLen = nestedCascadeOpt.length();
    size_t tryFlipOptLen = tryFlipOpt.length();
    string inputName; */
    tryflip = false;
    
    help();
    
    scale = 1;
    
    cascade.load(cascadeName);

    cv::namedWindow(OPENCV_WINDOW);
  }

  ~ImageConverter()
  {
    cv::destroyWindow(OPENCV_WINDOW);
  }

  void imageCb(const sensor_msgs::ImageConstPtr& msg)
  {
    cv_bridge::CvImagePtr cv_ptr;
    try
    {
      cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

    detectAndDraw( cv_ptr->image, cascade, nestedCascade, scale, tryflip );
    cv::waitKey(3);
    
    // Output modified video stream
    image_pub_.publish(cv_ptr->toImageMsg());
  }
  
  void detectAndDraw( Mat& img, CascadeClassifier& cascade,
                      CascadeClassifier& nestedCascade,
                      double scale, bool tryflip )
  {
    int i = 0;
    double t = 0;
    vector<Rect> faces, faces2;
    geometry_msgs::PointStamped center_msg;
    const static Scalar colors[] = { CV_RGB(0,0,255),
      CV_RGB(0,128,255),
      CV_RGB(0,255,255),
      CV_RGB(0,255,0),
      CV_RGB(255,128,0),
      CV_RGB(255,255,0),
      CV_RGB(255,0,0),
      CV_RGB(255,0,255)} ;
    Mat gray, smallImg( cvRound (img.rows/scale), cvRound(img.cols/scale), CV_8UC1 );
    
    cvtColor( img, gray, COLOR_BGR2GRAY );
    resize( gray, smallImg, smallImg.size(), 0, 0, INTER_LINEAR );
    equalizeHist( smallImg, smallImg );
    
    t = (double)cvGetTickCount();
    cascade.detectMultiScale( smallImg, faces,
      1.1, 3, 0
      //|CASCADE_FIND_BIGGEST_OBJECT
      //|CASCADE_DO_ROUGH_SEARCH
      |CASCADE_SCALE_IMAGE
      ,
      Size(min_face_size, min_face_size), 
      Size(max_face_size, max_face_size) );
    if(!faces.size()){
      min_face_size = 40;
      max_face_size = 100;
      center_msg.point.x = 0;
      center_msg.point.y = 0;
      center_msg.point.z = -1;
      center_msg.header.stamp = ros::Time::now();
      //Reset size
    }
    /*if( tryflip )
    {
      flip(smallImg, smallImg, 1);
      cascade.detectMultiScale( smallImg, faces2,
                                1.1, 2, 0
                                //|CASCADE_FIND_BIGGEST_OBJECT
                                //|CASCADE_DO_ROUGH_SEARCH
                                |CASCADE_SCALE_IMAGE
                                ,
                                Size(30, 30) );
      for( vector<Rect>::const_iterator r = faces2.begin(); r != faces2.end(); r++ )
      {
        faces.push_back(Rect(smallImg.cols - r->x - r->width, r->y, r->width, r->height));
      }
    }*/
    t = (double)cvGetTickCount() - t;
    printf( "detection time = %g ms\n", t/((double)cvGetTickFrequency()*1000.) );
    for( vector<Rect>::const_iterator r = faces.begin(); r != faces.end(); r++, i++ )
    {
      Mat smallImgROI;
      vector<Rect> nestedObjects;
      Point center;
      Scalar color = colors[i%8];
      int radius;
      min_face_size = MAX(40,(int)faces[0].width*0.85);
      max_face_size = MIN(100,(int)faces[0].height*1.15);
      
      double aspect_ratio = (double)r->width/r->height;
      if( 0.75 < aspect_ratio && aspect_ratio < 1.3 )
      {
        center.x = cvRound((r->x + r->width*0.5)*scale);
        center.y = cvRound((r->y + r->height*0.5)*scale);
        radius = cvRound((r->width + r->height)*0.25*scale);
        printf("Center at: %d, %d\n", center.x, center.y);
        printf("Size: %d\n",radius*2);
        circle( img, center, radius, color, 3, 8, 0 );
        center_msg.point.x = center.x;
        center_msg.point.y = center.y;
        center_msg.point.z = radius*2;
        center_msg.header.stamp = ros::Time::now();
      }
      else{
        rectangle( img, cvPoint(cvRound(r->x*scale), cvRound(r->y*scale)),
                   cvPoint(cvRound((r->x + r->width-1)*scale), cvRound((r->y + r->height-1)*scale)),
                   color, 3, 8, 0);
      }
      if( nestedCascade.empty() )
        continue;
      smallImgROI = smallImg(*r);
      nestedCascade.detectMultiScale( smallImgROI, nestedObjects,
        1.1, 2, 0
        //|CASCADE_FIND_BIGGEST_OBJECT
        //|CASCADE_DO_ROUGH_SEARCH
        //|CASCADE_DO_CANNY_PRUNING
        |CASCADE_SCALE_IMAGE
        ,
        Size(30, 30) );
      for( vector<Rect>::const_iterator nr = nestedObjects.begin(); nr != nestedObjects.end(); nr++ )
      {
        center.x = cvRound((r->x + nr->x + nr->width*0.5)*scale);
        center.y = cvRound((r->y + nr->y + nr->height*0.5)*scale);
        radius = cvRound((nr->width + nr->height)*0.25*scale);
        circle( img, center, radius, color, 3, 8, 0 );
      }
    }
    center_pub_.publish(center_msg);
    cv::imshow(OPENCV_WINDOW, img );
    }

};

int main(int argc, char** argv)
{
  ros::init(argc, argv, "image_converter");
  ImageConverter ic(argc, argv);
  ros::spin();
  return 0;
}
